//
//  ANTTableViewControllerInbox.h
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANTViewControllerAddTask.h"
#import "ANTTaskManager.h"

@interface ANTTableViewControllerInbox: UITableViewController <ANTViewControllerAddTaskDelegate, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
