//
//  ANTTaskManager.m
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTTaskManager.h"
#import "ANTTask.h"
@interface ANTTaskManager()
@property (nonatomic, strong) NSMutableArray *privateTaskList;
@end

@implementation ANTTaskManager

- (instancetype) init {
    if (self = [super init]) {
        _privateTaskList = [[NSMutableArray alloc] init];
        [self addTask:[[ANTTask alloc] initWithId:@"1" name:@"create task"]];
        [self addTask:[[ANTTask alloc] initWithId:@"2" name:@"do do do"]];
    }
    return self;
}

- (void)addTask:(ANTTask *)task {
    [self.privateTaskList addObject:task];
}

- (ANTTask*)taskByNum:(NSInteger)num {
    return self.privateTaskList[num];
}

- (NSInteger)numOfTasks {
    return [self.privateTaskList count];
}

@end
