//
//  ANTViewControllerDetailTask.h
//  toDoList
//
//  Created by Алекс  on 15.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANTViewControllerDetailTask : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) NSString *name;
@property (weak, nonatomic) IBOutlet UILabel *labelStartDate;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@end
