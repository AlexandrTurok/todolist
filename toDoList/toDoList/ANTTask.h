//
//  ANTTask.h
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANTTask : NSObject
@property (nonatomic, copy) NSString *taskID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSDate *startedAt;
@property (nonatomic, strong) NSDate *filishedAt;
@property (nonatomic, copy) NSString *note;

- (instancetype)initWithId:(NSString*)taskID name:(NSString*)name;

@end
