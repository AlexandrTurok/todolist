//
//  Constants.h
//  toDoList
//
//  Created by Алекс  on 10.07.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString *const ANTTableInboxIdentifier = @"tableInbox";
static NSString *const ANTSegueShowDetailTaskIdentifier = @"segueShowDetailTask";
static NSString *const ANTDateFormat = @"MMMM dd, yyyy HH:mm:ss";

#endif /* Constants_h */
