//
//  ANTViewControllerAddTask.m
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTViewControllerAddTask.h"
#import "ANTHelperDateFormatter.h"
#import "constants.h"

@interface ANTViewControllerAddTask ()
@end

@implementation ANTViewControllerAddTask

@synthesize delegate = _delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_task == nil) {
        self.task = [[ANTTask alloc] init];
    }

    UIBarButtonItem *buttonSave = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Save"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(saveButtonPressed)];

    self.navigationItem.rightBarButtonItem = buttonSave;
  
}
- (void)saveButtonPressed {
    self.task.name = self.textFieldName.text;
    self.task.note = self.textViewNotes.text;
    [self.delegate editTaskViewController:self didAddedTask:self.task];
}
- (IBAction)setDataButtonPressed:(UIButton *)sender {
    ANTViewControllerSetDate *vc = [[ANTViewControllerSetDate alloc] init];
    vc.delegate = self;
   [[self navigationController] pushViewController:vc animated:YES];
}
#pragma -mark delegate

- (void)editTaskViewController:(UIViewController *)vc didSetDate:(NSDate *)date {
    self.setDataButton.titleLabel.text =[[ANTHelperDateFormatter dataFormater:ANTDateFormat] stringFromDate:date];
    [self.task setStartedAt:date];
    [vc.navigationController popViewControllerAnimated:YES];
}

@end
