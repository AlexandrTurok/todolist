//
//  ANTViewControllerDetailTask.m
//  toDoList
//
//  Created by Алекс  on 15.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTViewControllerDetailTask.h"

@interface ANTViewControllerDetailTask ()

@end

@implementation ANTViewControllerDetailTask
@synthesize name = _name;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelName.text = self.name;
}


@end
