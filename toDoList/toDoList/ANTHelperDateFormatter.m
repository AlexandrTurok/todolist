//
//  ANTHelperDateFormatter.m
//  toDoList
//
//  Created by Алекс  on 09.07.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTHelperDateFormatter.h"

@implementation ANTHelperDateFormatter
+ (NSDateFormatter *)dataFormater:(NSString *)dateFormatt {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:dateFormatt];
    return format;
}
@end
