//
//  ANTViewControllerAddTask.h
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANTTask.h"
#import "ANTViewControllerSetDate.h"

@protocol ANTViewControllerAddTaskDelegate <NSObject>

- (void)editTaskViewController:(UIViewController *)vc didAddedTask:(ANTTask *)task;

@end

@interface ANTViewControllerAddTask : UIViewController <ANTViewControllerSetDateDelegat>

@property (nonatomic, strong) ANTTask *task;
@property (nonatomic, weak) id<ANTViewControllerAddTaskDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextView *textViewNotes;
@property (weak, nonatomic) IBOutlet UIButton *setDataButton;

- (void)save;

@end
