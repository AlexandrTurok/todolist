//
//  ANTViewControllerSetDate.h
//  toDoList
//
//  Created by Алекс  on 13.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ANTViewControllerSetDateDelegat <NSObject>

- (void)editTaskViewController:(UIViewController *)vc didSetDate:(NSDate *)date;

@end

@interface ANTViewControllerSetDate : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *dataPicker;
@property (nonatomic, weak) id<ANTViewControllerSetDateDelegat> delegate;


@end
