//
//  AppDelegate.h
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

