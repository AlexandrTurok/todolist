//
//  ANTTask.m
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTTask.h"

@implementation ANTTask

@synthesize name = _name;

- (instancetype)initWithId:(NSString*)taskID name:(NSString*)name {
    if(self = [super init]){
        _taskID = taskID;
        _name = name;
    }
    return self;
}

@end
