//
//  ANTViewControllerSetDate.m
//  toDoList
//
//  Created by Алекс  on 13.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import "ANTViewControllerSetDate.h"

@interface ANTViewControllerSetDate ()

@end

@implementation ANTViewControllerSetDate

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *buttonDone = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(doneButtonPressed)];
    
    self.navigationItem.rightBarButtonItem = buttonDone;
}

- (void)doneButtonPressed {
    [self.delegate editTaskViewController:self didSetDate:self.dataPicker.date];
}

@end
