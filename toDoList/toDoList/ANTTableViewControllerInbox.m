//
//  ANTTableViewControllerInbox.m
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//


#import "ANTTableViewControllerInbox.h"
#import "ANTViewControllerDetailTask.h"
#import "ANTHelperDateFormatter.h"
#import "Constants.h"

@interface ANTTableViewControllerInbox   ()
@property (nonatomic, strong) ANTTaskManager *taskManager;
@end

@implementation ANTTableViewControllerInbox

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.taskManager = [[ANTTaskManager alloc] init];
    

}

- (IBAction)buttonAddTaskTapped:(UIBarButtonItem *)sender {
    ANTViewControllerAddTask *addTaskController = [ANTViewControllerAddTask new];
    addTaskController.delegate = self;
    [[self navigationController ] pushViewController:addTaskController animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.taskManager numOfTasks];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ANTTableInboxIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.taskManager taskByNum:indexPath.row] name];
    return cell;
}

#pragma -mark delegate

- (void)editTaskViewController:(UIViewController *)vc didAddedTask:(ANTTask *)task {
    [self.taskManager addTask:task];
    [self.tableView reloadData];
    [vc.navigationController popViewControllerAnimated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:ANTSegueShowDetailTaskIdentifier
         ]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ANTViewControllerDetailTask *destViewController = segue.destinationViewController;
        destViewController.name = [[self taskManager] taskByNum:indexPath.row].name;
        destViewController.detailTextView.text =[[self taskManager] taskByNum:indexPath.row].note;

        destViewController.labelStartDate.text =[[ANTHelperDateFormatter dataFormater:ANTDateFormat] stringFromDate:[[self taskManager] taskByNum:indexPath.row].startedAt];

    }
}



@end
