//
//  ANTTaskManager.h
//  toDoList
//
//  Created by Алекс  on 04.06.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <Foundation/Foundation.h>
@class ANTTask;

@interface ANTTaskManager : NSObject
@property (nonatomic, strong) NSArray *taskList;

- (ANTTask*)taskById:(NSString*)taskId;
- (ANTTask*)taskByNum:(NSInteger)num;
- (void)addTask:(ANTTask*)task;
- (void)remove:(ANTTask *)taskId;
- (NSInteger)numOfTasks;

@end
