//
//  ANTHelperDateFormatter.h
//  toDoList
//
//  Created by Алекс  on 09.07.17.
//  Copyright © 2017 Алекс . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANTHelperDateFormatter : NSObject
+ (NSDateFormatter *)dataFormater:(NSString *)dateFormatt;
@end
